//
//  ViewController.m
//  Fawry
//
//  Created by Mohamed Atta on 11/4/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize fawrytypesInstance,timer,web,manager,mapInstance;
@synthesize btnaddress,btncity,btnid,btnnearest,fawryimage;

-(void)resize
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    
    if(screenHeight<screenWidth) // rotation
    {
        NSLog(@"reframe");
        
        int height=screenHeight/4;
        int width=screenWidth/3;
        
        int x=screenWidth/6,y=0;// constant for navigation height
        y+=  [UIApplication sharedApplication].statusBarFrame.size.height;
        y+=self.navigationController.navigationBar.frame.size.height;
        
        
        [self.view setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
        
        
        y+=20;
        
        
        [fawryimage setFrame:CGRectMake(screenWidth/3, y, width, height)];
        y+=height+10;// for padding
        
        
        [btnnearest setFrame:CGRectMake(x, y, width, height)];
        
        y+=height+10;// for padding
        
        
        [btncity setFrame:CGRectMake(x, y, width, height)];
        y+=height+10;// for padding
        
        
        [btnaddress setFrame:CGRectMake(btnnearest.frame.origin.x+screenWidth/3+10 , btnnearest.frame.origin.y, width, height)];
        y+=height+10;// for padding
        
        [btnid setFrame:CGRectMake(btncity.frame.origin.x+screenWidth/3+10, btncity.frame.origin.y, width, height)];
        
        
        
        return;
    }
    
    
    int height=screenHeight/8;
    int width=screenWidth*2/3;
    
    int x=screenWidth/6,y=0;// constant for navigation height
    y+=  [UIApplication sharedApplication].statusBarFrame.size.height;
    y+=self.navigationController.navigationBar.frame.size.height;
    
    
    [self.view setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    
    
    y+=20;
    
    
    [fawryimage setFrame:CGRectMake(x, y, width, height)];
    y+=height+10;// for padding
    
    
    [btnnearest setFrame:CGRectMake(x, y, width, height)];
    
    y+=height+10;// for padding
    
    
    [btncity setFrame:CGRectMake(x, y, width, height)];
    y+=height+10;// for padding
    
    
    [btnaddress setFrame:CGRectMake(x, y, width, height)];
    y+=height+10;// for padding
    
    
    [btnid setFrame:CGRectMake(x, y, width, height)];
    
    
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self resize];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"فوري جنبك"];
    // Do any additional setup after loading the view, typically from a nib.
    manager=[[CLLocationManager alloc]init];
    
    // right bar layout
    UIImage *refreshimage=[UIImage imageNamed:@"menuicon.png"];
    refreshimage=[MapLocations imageWithImage:refreshimage scaledToSize:CGSizeMake(30, 35)];
    
    UIBarButtonItem *about=[[UIBarButtonItem alloc]initWithImage:refreshimage style:UIBarButtonItemStylePlain target:self action:@selector(showActionSheet)];
    self.navigationItem.rightBarButtonItem=about;
    
    [self resize];
    
    
    
    
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btnNearest:(id)sender {
    fawrytypesInstance=[self.storyboard instantiateViewControllerWithIdentifier:@"FawryTypes"];
    [fawrytypesInstance setCallertype:@"nearest"];
    [self.navigationController pushViewController:fawrytypesInstance animated:true];
    
}

- (IBAction)btnSearchAddress:(id)sender {


     UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"ادخل العنوان " delegate:self cancelButtonTitle:@"رجوع"  otherButtonTitles:@"موافق", nil];
   
    [alert setTag:1];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].delegate=self;
    [alert show];
    
}

- (IBAction)btnSearchID:(id)sender {

    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"ادخل رقم الحساب " delegate:self cancelButtonTitle:@"رجوع"  otherButtonTitles:@"موافق", nil];
    [alert setTag:2];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].delegate=self;
    
    [alert show];
    

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UITextField *textField = [alertView textFieldAtIndex:0];
    [textField resignFirstResponder];
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{

    
    
    
    if([alertView tag]==1){
    if (buttonIndex == 1) { // seearch
 NSString *address=[[alertView textFieldAtIndex:0] text];

        NSString *searchin=@"country:EG";
        NSMutableString *googleMapsAPI =[[NSMutableString alloc]initWithString:@"http://maps.google.com/maps/api/geocode/json?"];

        [googleMapsAPI appendString:@"components="];
        [googleMapsAPI appendString:searchin];
        [googleMapsAPI appendString:@"&address="];
        [googleMapsAPI appendString:[address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        
        web=[[WebServiceRequests alloc]initWithUrl:googleMapsAPI];
        
        
        timer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(checkSearchaddress) userInfo:self repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        
    
    
    
    }
}
    else if ([alertView tag]==2)
    {

        if (buttonIndex == 1) { // seearch

        NSString *terid=[[alertView textFieldAtIndex:0] text];
         
            terid=[terid stringByReplacingOccurrencesOfString:@"٠" withString:@"0"];
            terid=[terid stringByReplacingOccurrencesOfString:@"١" withString:@"1"];
            terid=[terid stringByReplacingOccurrencesOfString:@"٢" withString:@"2"];
            terid=[terid stringByReplacingOccurrencesOfString:@"٣" withString:@"3"];
            terid=[terid stringByReplacingOccurrencesOfString:@"٤" withString:@"4"];
            terid=[terid stringByReplacingOccurrencesOfString:@"٥" withString:@"5"];
            terid=[terid stringByReplacingOccurrencesOfString:@"٦" withString:@"6"];
            terid=[terid stringByReplacingOccurrencesOfString:@"٧" withString:@"7"];
            terid=[terid stringByReplacingOccurrencesOfString:@"٨" withString:@"8"];
            terid=[terid stringByReplacingOccurrencesOfString:@"٩" withString:@"9"];
            
        NSMutableString *searchbyid=[[NSMutableString alloc]initWithString:@"http://sacred-store-100115.appspot.com/API/findBranchByTerminalNo?"];
        
        [searchbyid appendString:@"terminalno="];
        [searchbyid appendString:[terid stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        web=[[WebServiceRequests alloc]initWithUrl:searchbyid];
        
        timer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(checksearchid) userInfo:self repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        
        
        

        
        
    
    
        }}

}

- (IBAction)btnSearchCity:(id)sender {
    if(timer!=nil) // this mean that an timer instance is running
        return;

    web=[[WebServiceRequests alloc]initWithUrl:@"https://sacred-store-100115.appspot.com/API/getDistinctCities"];
    
    timer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(check) userInfo:self repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    

}

-(void)check
{
    if(web.IsLoaded==2)// then error occur
    {
        [timer invalidate];
        timer=nil;
    }
    
    if(web.IsLoaded==1){
        [timer invalidate];
        timer=nil;
        NSDictionary *dict=web.datadic;
        NSArray *locations=[dict objectForKey:@"data"];
        SearchCity *searchcity=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchCity"];
        [searchcity setPlaces:locations];
     //   NSLog(@"%@",[[searchcity places]objectAtIndex:0]);
        [self.navigationController pushViewController:searchcity animated:true];
    }
    
}

-(void)checksearchid
{
    if(web.IsLoaded==2)// then error occur
    {
        [timer invalidate];
        timer=nil;
        }
        
    if(web.IsLoaded==1){
        [timer invalidate];
        timer=nil;
        NSArray *locations=[NSJSONSerialization JSONObjectWithData:[web Data] options:kNilOptions error:nil];

        if([locations count]==0)
        {
            
            [[UIAlertView alloc]initWithTitle:@"" message:@"لا يوجد فروع." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil].show;
            return;
        }

        mapInstance=[self.storyboard instantiateViewControllerWithIdentifier:@"MapLocations"];
        [mapInstance setCallertype:@"terminalid"];
        [mapInstance setFawrylocations:locations];
        [self.navigationController pushViewController:mapInstance animated:true];
        
    }
    
}



-(void)checkSearchaddress
{
    if(web.IsLoaded==2)// then error occur
    {
        [timer invalidate];
        timer=nil;
    }
    
    if(web.IsLoaded==1){
        [timer invalidate];
        NSDictionary *dict=web.datadic;
        NSString *lon=[[[[[[dict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"location" ] objectForKey:@"lng"] stringValue];
        NSString *lat=[[[[[[dict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"location" ] objectForKey:@"lat"] stringValue];
        
        
        
        
        
        fawrytypesInstance=[self.storyboard instantiateViewControllerWithIdentifier:@"FawryTypes"];
        [fawrytypesInstance setCallertype:@"address"];
        [fawrytypesInstance setLat:lat];
        [fawrytypesInstance setLon:lon];
        
        
        [self.navigationController pushViewController:fawrytypesInstance animated:true];
   }
    
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{

    [self resize];
  

}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
}



 -(void)showActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:Nil delegate:self cancelButtonTitle:@"رجوع" destructiveButtonTitle:nil otherButtonTitles:@"الصفحة الرئيسية", @"من نحن ؟", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault; [actionSheet showInView:self.view];

}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    
    
    switch (buttonIndex)
    {
        case 0:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case 1:
        {
            about *aboutinstance=[self.storyboard instantiateViewControllerWithIdentifier:@"about"];
            [self.navigationController presentModalViewController:aboutinstance animated:YES];
        }
            break;
            
    }
}
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    UITextField *textField = [alertView textFieldAtIndex:0];
    if ([textField.text length] == 0){

        return NO;
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return false;}

@end
