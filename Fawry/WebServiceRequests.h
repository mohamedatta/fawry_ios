//
//  WebServiceRequests.h
//  Fawry
//
//  Created by Mohamed Atta on 11/5/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Reachability.h"
@interface WebServiceRequests : NSObject<NSURLConnectionDataDelegate>
{
     UIAlertView *alert;
    NSURLConnection * connection;
}
@property(strong,atomic)NSDictionary *datadic;
@property(strong,nonatomic)NSMutableData *Data;
@property(strong,atomic)NSString *url;
@property int IsLoaded;
- (id) initWithUrl: (NSString*) URL;

@end
