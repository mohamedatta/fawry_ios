//
//  SearchCity.h
//  Fawry
//
//  Created by Mohamed Atta on 11/4/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServiceRequests.h"
#import "FawryTypes.h"
@interface SearchCity : UITableViewController<UIActionSheetDelegate>
@property(strong,atomic)NSArray *places;
@property(strong,nonatomic)NSString *province;
@property(strong,nonatomic)NSString *city;
@property(strong,nonatomic)NSString *district;
-(void)check;
@property(strong,atomic)NSTimer *timer;
@property(strong,atomic)WebServiceRequests *web;
@property (strong, nonatomic) IBOutlet UITableView *table;

@end
