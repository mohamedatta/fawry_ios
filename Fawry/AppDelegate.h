//
//  AppDelegate.h
//  Fawry
//
//  Created by Mohamed Atta on 11/4/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic) ViewController *menu;

@end

