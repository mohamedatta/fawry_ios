//
//  SearchCity.m
//  Fawry
//
//  Created by Mohamed Atta on 11/4/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import "SearchCity.h"

@interface SearchCity ()

@end

@implementation SearchCity
static int path=0;
static NSString *city=@"";
static NSString *area=@"";
static NSString *district=@"";
@synthesize places,web,timer,table;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set about
    // right bar layout
    UIImage *refreshimage=[UIImage imageNamed:@"menuicon.png"];
    refreshimage=[MapLocations imageWithImage:refreshimage scaledToSize:CGSizeMake(30, 35)];
    
    UIBarButtonItem *about=[[UIBarButtonItem alloc]initWithImage:refreshimage style:UIBarButtonItemStylePlain target:self action:@selector(showActionSheet)];
    self.navigationItem.rightBarButtonItem=about;

    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    if(path==0)
    [self setTitle:@"المحافظه"];
else if(path==1)
    [self setTitle:@"المدينه"];
else if(path==2)
    [self setTitle:@"الحي"];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    int height=screenHeight/8;
    int width=screenWidth*2/3;
    
    [self.view setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [self.table setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [places count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text=[places objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment=NSTextAlignmentCenter;
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.backgroundColor=[UIColor clearColor];

    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(timer!=nil) // this mean that an timer instance is running
        return;

    
    NSMutableString *tempurl=[[NSMutableString alloc]initWithString:@"http://sacred-store-100115.appspot.com/API/"];
    if(path==0)
    {
        city=[places objectAtIndex:indexPath.row];
        [tempurl appendString:@"getAreas?"];
    
        [tempurl appendString:@"city="];
        [tempurl appendString:[city stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        

    }
    else if(path==1)
    {   area=[places objectAtIndex:indexPath.row];
        [tempurl appendString:@"getDistricts?"];
        [tempurl appendString:@"city="];
        [tempurl appendString:[city stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [tempurl appendString:@"&area="];
        [tempurl appendString:[area stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
    }
// navigate to select the type of the service
if(path==2)
{
    district=[places objectAtIndex:indexPath.row];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:city forKey:@"city"];
    [defaults setObject:area forKey:@"area"];
    [defaults setObject:district forKey:@"district"];
    
    [defaults synchronize];
    
    FawryTypes *fawrytype=[self.storyboard instantiateViewControllerWithIdentifier:@"FawryTypes"];
    [fawrytype setCallertype:@"searchCity"];
    [self.navigationController pushViewController:fawrytype animated:true];


}
else{
    web=[[WebServiceRequests alloc]initWithUrl:tempurl];
    
    timer=[NSTimer timerWithTimeInterval:.5 target:self selector:@selector(check) userInfo:self repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
}

}

-(void)check
{
    if(web.IsLoaded==2)// then error occur
    {
        [timer invalidate];
        timer=nil;
    }
    
    if(web.IsLoaded==1){
        path++;
        
        [timer invalidate];
        timer=nil;

        NSDictionary *dict=web.datadic;
        NSArray *locations=[dict objectForKey:@"data"];
        SearchCity *searchcity=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchCity"];
        [searchcity setPlaces:locations];
        [self.navigationController pushViewController:searchcity animated:true];
    }
    
}

- (void)didMoveToParentViewController:(UIViewController *)parent
{
    if (![parent isEqual:self.parentViewController]) {
      //  NSLog(@"Back pressed");
        path--;
        if(path<0)
            path=0;
    }
}



-(void)showActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:Nil delegate:self cancelButtonTitle:@"رجوع" destructiveButtonTitle:nil otherButtonTitles:@"الصفحة الرئيسية", @"من نحن ؟", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault; [actionSheet showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case 1:
        {
            about *aboutinstance=[self.storyboard instantiateViewControllerWithIdentifier:@"about"];
            [self.navigationController presentModalViewController:aboutinstance animated:YES];
        }
            break;
            
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
