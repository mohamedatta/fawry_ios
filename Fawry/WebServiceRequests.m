//
//  WebServiceRequests.m
//  Fawry
//
//  Created by Mohamed Atta on 11/5/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import "WebServiceRequests.h"

@implementation WebServiceRequests
@synthesize Data,datadic,url,IsLoaded;

- (id) initWithUrl: (NSString*) URL
{
    IsLoaded=0;
    self = [super init];
    if (self) {
       
        
        
        IsLoaded=0;
        alert=[[UIAlertView alloc] initWithTitle:@"" message:@"جاري التحميل ، انتظر من فضلك..." delegate:self cancelButtonTitle:nil  otherButtonTitles: nil];
        UIActivityIndicatorView *progress=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
       UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0
                                                             , 0,50, 50)];
        
        progress.frame=CGRectMake(view.bounds.size.width+90
                                  , view.bounds.size.height/2, 0,0);
        
        [view addSubview:progress];
       // NSLog(@"%f",alert.bounds.size.width);
        [progress startAnimating];
        [alert setValue:view forKey:@"accessoryView"];
      //  [progress setCenter:CGPointMake(alert.frame.origin.x, alert.frame.origin.y)];
        progress.transform=CGAffineTransformMakeScale(2, 2);
        [alert show];
   
        // All initializations, for example:
       // NSLog(@"%@",URL);
        [self setUrl:URL];
        
        NSURL *nsurl=[[NSURL alloc]initWithString:url];
        
        NSMutableURLRequest *request=[[NSMutableURLRequest alloc] initWithURL:nsurl];
        [request setHTTPMethod:@"POST"];
       [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        connection=[[NSURLConnection alloc] initWithRequest:request delegate:self];

        [connection start];
       // NSLog(@"connection start");
    }
   // NSLog(@"ohhhhhhhhhhhhhhhhhhh my god");
    return self;
}
- (BOOL)networkChanged:(NSNotification *)notification
{
    
    Reachability * reachability = [notification object];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
       // NSLog(@"Not Reachable");
        [alert dismissWithClickedButtonIndex:-1 animated:true];
        [connection cancel];
    
        
        [[UIAlertView alloc]initWithTitle:@"تحذير" message:@"لا يوجد اتصال بالانترنت." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil].show;
        
 
        return  false;
    }
    else if (remoteHostStatus == ReachableViaWiFi) {
        NSLog(@"Reachable Wifi");
    }
    else if (remoteHostStatus == ReachableViaWWAN) {
        NSLog(@"Reachable WWAN");
    }
    return true;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
   // NSLog(@"start");
    
    Data =[[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [Data appendData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
   // NSLog(@"%@",    [[NSString alloc] initWithData:Data encoding:NSUTF8StringEncoding]);
   [alert dismissWithClickedButtonIndex:-1 animated:true];

    datadic=[NSJSONSerialization JSONObjectWithData:Data options:kNilOptions error:nil];
    
    IsLoaded=1;

}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    IsLoaded=2;//// to indicate error;
   // NSLog(@"test errrrrrrrrrrrror  %@",error);
    [alert dismissWithClickedButtonIndex:-1 animated:true];
    [[UIAlertView alloc]initWithTitle:@"تحذير" message:@"من فضلك تآكد من وجود اتصال بالانترنت." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil].show;
    
    
}

@end
