//
//  ViewController.h
//  Fawry
//
//  Created by Mohamed Atta on 11/4/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "FawryTypes.h"
#import "WebServiceRequests.h"
#import "SearchCity.h"
#import "Reachability.h"
#import "about.h"
@interface ViewController : UIViewController<CLLocationManagerDelegate,UIActionSheetDelegate,UITextFieldDelegate,UIAlertViewDelegate>

- (IBAction)btnSearchCity:(id)sender;
@property(strong,atomic)FawryTypes *fawrytypesInstance;
@property(weak,atomic)MapLocations *mapInstance;
@property (weak, nonatomic) IBOutlet UIImageView *fawryimage;
@property (weak, nonatomic) IBOutlet UIButton *btnnearest;
@property (weak, nonatomic) IBOutlet UIButton *btncity;
@property (weak, nonatomic) IBOutlet UIButton *btnaddress;
@property (weak, nonatomic) IBOutlet UIButton *btnid;

- (IBAction)btnNearest:(id)sender;
- (IBAction)btnSearchAddress:(id)sender;
- (IBAction)btnSearchID:(id)sender;
-(void)check;
@property(strong,atomic)NSTimer *timer;
@property(strong,atomic)CLLocationManager *manager;

@property(strong,atomic)WebServiceRequests *web;
@end

