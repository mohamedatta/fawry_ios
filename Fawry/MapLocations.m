//
//  MapLocations.m
//  Fawry
//
//  Created by Mohamed Atta on 11/8/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import "MapLocations.h"
@implementation MapLocations
UIBarButtonItem *startsearch;
bool targetlocation=false;
bool targetInserted=false;
float targetlong;
float targetlat;
MKCoordinateRegion cordinateRegion;
@synthesize fawrylocations,map,merchanttype,phone,terminalid,mylat,mylong,callertype,fawrytype;
@synthesize timer,web,manager,note;

int iterator=0;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    int height=screenHeight/8;
    int width=screenWidth*2/3;
    int y=0;
    int statusbar=MIN([UIApplication sharedApplication].statusBarFrame.size.height, [UIApplication sharedApplication].statusBarFrame.size.width);
    
    int navbar=self.navigationController.navigationBar.frame.size.height;
    
    [self.view setFrame:CGRectMake(0, y, screenWidth, screenHeight-y)];
    [self.map setFrame:CGRectMake(0, y, screenWidth, screenHeight-y)];
    [self.note setFrame:CGRectMake(0, statusbar+navbar+5, screenWidth, 10)];
    [note setTextColor:[UIColor blueColor]];
    [map setRegion:cordinateRegion animated:YES];
    
    
    
    
}

-(void)dealloc
{
    
   // NSLog(@"dealloc");
    map.delegate=nil;
    map.mapType = MKMapTypeHybrid;
    
    map.showsUserLocation=NO;
    [map removeAnnotations:[map annotations]];
    
    [map removeFromSuperview];
    fawrylocations=nil;
    map=nil;
}


- (void)viewDidLoad
{
    [super viewDidLoad];



    
    
    
    targetlocation=false;
    targetInserted=false;
    // set about
    // right bar layout
    UIImage *aboutimage=[UIImage imageNamed:@"menuicon.png"];
    aboutimage=[MapLocations imageWithImage:aboutimage scaledToSize:CGSizeMake(30, 35)];
    
    UIBarButtonItem *about=[[UIBarButtonItem alloc]initWithImage:aboutimage style:UIBarButtonItemStylePlain target:self action:@selector(showActionSheet)];
    

    
    
    // right bar layout
    UIImage *homeimage=[UIImage imageNamed:@"home.png"];
    homeimage=[MapLocations imageWithImage:homeimage scaledToSize:CGSizeMake(30, 35)];
    
    UIBarButtonItem *home=[[UIBarButtonItem alloc]initWithImage:homeimage style:UIBarButtonItemStylePlain target:self action:@selector(gotoHomePage)];
    UIImage *refreshimage=[UIImage imageNamed:@"ref.png"];
    refreshimage=[MapLocations imageWithImage:refreshimage scaledToSize:CGSizeMake(30, 35)];
    
    UIBarButtonItem *refresh=[[UIBarButtonItem alloc]initWithImage:refreshimage style:UIBarButtonItemStylePlain target:self action:@selector(Refresh)];
  
    
    if([callertype isEqualToString:@"nearest"])
    self.navigationItem.rightBarButtonItems=@[about,home,refresh];
else
    self.navigationItem.rightBarButtonItems=@[about,home];
    
    // left bar layout
    self.navigationItem.leftItemsSupplementBackButton=true;
    startsearch=[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(startSearch)];
    
    self.navigationItem.leftBarButtonItem=startsearch;
    [startsearch setEnabled:false];
    
    int i=1;
    iterator=0;
    if([callertype isEqualToString:@"targetlocation"]||[callertype isEqualToString:@"nearest"]|| [callertype isEqualToString:@"address"])
    {       i=0;
        iterator=-1;
    }

    
   
    for ( i; i < [fawrylocations count] ; i++) {
        NSDictionary *dictlocation=[fawrylocations objectAtIndex:i];
        
        MKPointAnnotation *fawry=[MKPointAnnotation new];

        // set title as index in the array
        [fawry setTitle:[NSString stringWithFormat:@"%d", i]];
        
        [fawry setCoordinate:CLLocationCoordinate2DMake([[dictlocation objectForKey:@"lat"] doubleValue], [[dictlocation objectForKey:@"long"] doubleValue])];
        
        
        [map addAnnotation:fawry];
        
        
        
        
        
    
    }

    
    
  //     [map setCenterCoordinate:CLLocationCoordinate2DMake([[centerlocation objectForKey:@"centerLat"] doubleValue], [[centerlocation objectForKey:@"centerLon"] doubleValue])];
    [map setZoomEnabled:YES];
    //    [map setVisibleMapRect:MKMapRectWorld];
    
    MKCoordinateSpan span;
    //You can set span for how much Zoom to be display like below
    span.latitudeDelta=.05;
    span.longitudeDelta=.05;
    
    //set Region to be display on MKMapView
    
    
    
    // if nearest branches
    if([callertype isEqualToString:@"targetlocation"]||[callertype isEqualToString:@"nearest"]|| [callertype isEqualToString:@"address"])
    {    cordinateRegion.center.latitude=[mylat doubleValue];
    cordinateRegion.center.longitude=[mylong doubleValue];
    
    }
    else {
        // if search by city
        
        NSDictionary *centerlocation=[fawrylocations objectAtIndex:0];
        
        cordinateRegion.center.latitude=[[centerlocation objectForKey:@"centerLat"] doubleValue];
        cordinateRegion.center.longitude=[[centerlocation objectForKey:@"centerLon"] doubleValue];
    
    
    }
    
    
    
    map.showsUserLocation =true;
    cordinateRegion.span=span;
  
    
    // add uitabgesuture to handle user tab touch locations
  //  UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
   // [map addGestureRecognizer:tapGesture];
 /*   UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] init];
    [doubleTap setNumberOfTapsRequired:2];
    [doubleTap setCancelsTouchesInView:NO];
    [map addGestureRecognizer:doubleTap];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [tapGesture requireGestureRecognizerToFail:doubleTap];
    [map addGestureRecognizer:tapGesture];
   */
    
    
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleTap:)];
    lpgr.minimumPressDuration = 0.5; //
    [self.map addGestureRecognizer:lpgr];
   
}














- (void)handleTap:(id)sender
{

    
    UILongPressGestureRecognizer *tapGesture = (UILongPressGestureRecognizer*)sender;
    if (tapGesture.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint tapPoint = [tapGesture locationInView:map];
    CLLocationCoordinate2D coord = [map convertPoint:tapPoint toCoordinateFromView:map];
    
//    NSUInteger numberOfTouches = [tapGesture numberOfTouches];
   // NSLog(@"before %d",[[map annotations]count]);
    
    for (int iter=0; iter<[[map annotations]count]; iter++) {
        if([[[[map annotations]objectAtIndex:iter] title]isEqualToString:@"Target Location"])
        {[map removeAnnotation:[[map annotations]objectAtIndex:iter]];
        iter--;}
        
    }
   
    NSLog(@"after %lu",(unsigned long)[[map annotations]count]);
    
    [startsearch setEnabled:true];
        [startsearch setTitle:@"بدآ البحث بالموقع الجديد"];
       targetlocation=true;
    
    MKPointAnnotation *fawry=[MKPointAnnotation new];
        [fawry setTitle:@"Target Location"];
        
        [fawry setCoordinate:CLLocationCoordinate2DMake(coord.latitude, coord.longitude)];
    targetlat=coord.latitude ;
    targetlong=coord.longitude;
    
    
        [map addAnnotation:fawry];

        
    targetInserted=true;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   // NSLog(@"memory warning");
    // Dispose of any resources that can be recreated.
    
}

-(void)Refresh
{

    if(timer==nil ){
    manager=[[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [manager requestAlwaysAuthorization];
    [manager requestWhenInUseAuthorization];
 
    [manager startUpdatingLocation];
    
    callertype =@"nearest";
    }
    
    
    
    
    
    
    
}


-(void)startSearch
{

    FawryTypes *fawryinstance=[self.storyboard instantiateViewControllerWithIdentifier:@"FawryTypes"];
    [fawryinstance setLat:[NSString stringWithFormat:@"%f",targetlat]];
    [fawryinstance setLon:[NSString stringWithFormat:@"%f",targetlong]];
    [fawryinstance setCallertype:@"targetlocation"];
    
    
    //[self.navigationController pushViewController:fawryinstance animated:YES];
    
    
    
    NSArray *oldviewControllers = self.navigationController.viewControllers;
    NSMutableArray *newViewControllers = [NSMutableArray array];
    
    // preserve the root view controller
    [newViewControllers addObject:[oldviewControllers objectAtIndex:0]];
    // add the new view controller
    [newViewControllers addObject:fawryinstance];
    // animatedly change the navigation stack
    [self.navigationController setViewControllers:newViewControllers animated:YES];

    
    
    
}
-(void)gotoHomePage
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
    
}

-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    for (UIView *subview in view.subviews) {
        if (![subview isKindOfClass:[UIView class]]) {
            continue;
        }
        
        [subview removeFromSuperview];
    }
    
}
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
   // NSLog(@"select");
    UIView *callout=view.rightCalloutAccessoryView;
    callout.center=CGPointMake(view.bounds.size.width*0.5f, -callout.bounds.size.height*0.5f);
    cordinateRegion.center.latitude=[view.annotation coordinate].latitude;
    cordinateRegion.center.longitude=[view.annotation coordinate].longitude;
    cordinateRegion.span=[mapView region].span;
    [mapView setRegion:cordinateRegion animated:YES];
    [view addSubview:callout];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        
        MKAnnotationView *pinView;
        if([[annotation title] isEqualToString:@"Target Location"])
        {
            pinView= (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"TargetAnnotationView"];
          
              if (!pinView)
              {
                  
                  
                  if(targetlocation==true||[[annotation title] isEqualToString:@"Target Location"])
                  {

                      pinView= [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"TargetAnnotationView"];
                      pinView.canShowCallout = YES;
                      
                      pinView.calloutOffset = CGPointMake(0, 32);
                      
                      pinView.image = [UIImage imageNamed:@"bluepin.png"] ;
                      pinView.image=[MapLocations imageWithImage:pinView.image scaledToSize:CGSizeMake(25, 30)];
                      targetlocation=false;
                      
                      
                      
                      
                      return pinView;
                      
                  }
                  
          
          
          
          
          
          }
          else{
                  pinView.annotation=annotation;
                  pinView.canShowCallout = YES;
              
              [pinView.annotation coordinate].latitude;
              [pinView.annotation coordinate].longitude;
              pinView.calloutOffset = CGPointMake(0, 32);
                  pinView.rightCalloutAccessoryView=nil;
                  pinView.leftCalloutAccessoryView=nil;
                  
                  pinView.image = [UIImage imageNamed:@"bluepin.png"] ;
                  pinView.image=[MapLocations imageWithImage:pinView.image scaledToSize:CGSizeMake(25, 30)];
                  targetlocation=false;
                  
                  return pinView;
                  
              
              }
          
        }
        
          else
        {
          pinView= (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotationView"];
          
          if (!pinView)
          {
          
              // If an existing pin view was not available, create one.
              pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"PinAnnotationView"];
        
              
          }
           else
           {
               pinView.annotation = annotation;
               
           }
              
              pinView.image = [UIImage imageNamed:@"mappin.png"] ;
              pinView.canShowCallout = NO;
              pinView.calloutOffset = CGPointMake(0, 32);
              pinView.image=[MapLocations imageWithImage:pinView.image scaledToSize:CGSizeMake(25, 30)];
              
              
              NSDictionary *dictlocation=[fawrylocations objectAtIndex:[[annotation title] intValue]];
              [dictlocation objectForKey:@"name"];
              phone=[dictlocation objectForKey:@"phone"];
              merchanttype=[[NSMutableString alloc]initWithString:[dictlocation objectForKey:@"address"]];
              
              terminalid=[[NSMutableString alloc]initWithString: @"Terminal ID: "];
              
              [terminalid appendString: [dictlocation objectForKey:@"TerminalFawryID"]];
              
              
              ///////////////////////////////////////////////// designing callout view
              
              UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0,150, 105)];
              
              view.autoresizesSubviews=YES;
              UILabel *title=[[UILabel alloc]initWithFrame:CGRectMake(5, 3,140, 20)];
              [title setText:[dictlocation objectForKey:@"name"]];
              [title setNumberOfLines:1];
              title.textAlignment=NSTextAlignmentCenter;
              title.adjustsFontSizeToFitWidth=YES;
              [view addSubview:title];
              UILabel *subtitle=[[UILabel alloc]initWithFrame:CGRectMake(5, 20,140, 20)];
              [subtitle setText:[dictlocation objectForKey:@"MerchantTypeName"]];
              [subtitle setNumberOfLines:1];
              subtitle.adjustsFontSizeToFitWidth=YES;
              subtitle.textAlignment=NSTextAlignmentCenter;
              
              [view addSubview:subtitle];
              
              UILabel *textr=[[UILabel alloc]initWithFrame:CGRectMake(5, 40,140, 20)];
              [textr setNumberOfLines:1];
              textr.textAlignment=NSTextAlignmentRight;
              
              [textr setText:merchanttype];
              [textr setBackgroundColor:[UIColor whiteColor]];
              textr.adjustsFontSizeToFitWidth=YES;
              [view addSubview:textr];
              UILabel *phonelabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 65,140, 15)];
              [phonelabel setNumberOfLines:1];
              
              
              [phonelabel setText:phone];
              [phonelabel setBackgroundColor:[UIColor whiteColor]];
              phonelabel.adjustsFontSizeToFitWidth=YES;
              [view addSubview:phonelabel];
              [view setBackgroundColor:[UIColor whiteColor]];
              
              UILabel *text=[[UILabel alloc]initWithFrame:CGRectMake(5, 80,140, 23)];
              [text setText:terminalid];
              
              text.adjustsFontSizeToFitWidth=YES;
              
              [text setNumberOfLines:1];
              [view addSubview:text];
              
              
              
              pinView.rightCalloutAccessoryView=view;
              
              ////////////////////////////////////////////////////
              
              
          
            return pinView;
  

        }

    }
    return nil;
}



+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView
{
  //  NSLog(@"1");
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
    //NSLog(@"2");
    
}
- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error
{
    //NSLog(@"3");
    
}

- (void)mapViewWillStartRenderingMap:(MKMapView *)mapView NS_AVAILABLE(10_9, 7_0){
   // NSLog(@"4");
    
}
- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered NS_AVAILABLE(10_9, 7_0)
{
   // NSLog(@"5");
    
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
   // NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"" message:@"تعذر الوصول لموقعك" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil];
    [errorAlert show];
}





-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
   // NSLog(@"didUpdateToLocation hhhh");
    int pos=[locations count];
    pos -=1;
    CLLocation *currentLocation = [locations objectAtIndex:pos];
    
    if (currentLocation != nil) {
        NSLog([NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]);
        NSLog([NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
        
        
    }
    

  
    [self.manager stopUpdatingLocation];
    self.manager=nil;
    NSMutableString *refreshurl=[[NSMutableString alloc]initWithString:@"http://sacred-store-100115.appspot.com/API/getBranchesByLocationAfterExtraFilters?"];
    [refreshurl appendString:@"type="];
    [refreshurl appendString:fawrytype];
    
    [refreshurl appendString:@"&lat="];
    [refreshurl appendString:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]];
    [refreshurl appendString:@"&lon="];
    [refreshurl appendString:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]];
    mylat=[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    mylong=[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    
    web=[[WebServiceRequests alloc]initWithUrl:refreshurl];
    
    timer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(refreshcheck) userInfo:self repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
    
    
    
    
    
}
-(void)refreshcheck
{

    if(web.IsLoaded==2)// then error occur
    {
        [timer invalidate];
        timer=nil;
    }
    
    if(web.IsLoaded==1){
        if([callertype isEqualToString:@"nearest"])
        {
            [timer invalidate];
            timer =nil;
            NSArray *locations=[NSJSONSerialization JSONObjectWithData:[web Data] options:kNilOptions error:nil];
            
            if([locations count]==0)
            {
                
                [[UIAlertView alloc]initWithTitle:@"" message:@"لا يوجد فروع." delegate:self cancelButtonTitle:nil otherButtonTitles:@"موافق", nil].show;
                return;
            }
            
            
            [map removeAnnotations:map.annotations];
            fawrylocations=locations;
            [self viewDidLoad];
            
            [self viewDidAppear:true];
    
            
            return;}
        
        
    
}

}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewDidAppear:true];
   // NSLog(@"ok");

}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
}




-(void)showActionSheet
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:Nil delegate:self cancelButtonTitle:@"رجوع" destructiveButtonTitle:nil otherButtonTitles:@"الصفحة الرئيسية", @"من نحن ؟", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault; [actionSheet showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case 1:
        {
            about *aboutinstance=[self.storyboard instantiateViewControllerWithIdentifier:@"about"];
            [self.navigationController presentModalViewController:aboutinstance animated:YES];
        }
            break;
            
    }
}

@end
