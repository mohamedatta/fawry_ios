//
//  FawryTypes.m
//  Fawry
//
//  Created by Mohamed Atta on 11/4/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import "FawryTypes.h"

@interface FawryTypes ()

@end

@implementation FawryTypes
NSMutableString *nearesturl;
NSString *fawrytype;

@synthesize types,typecode,web,timer,manager,callertype,lon,lat,terminalid;
- (void)viewDidLoad {
    [super viewDidLoad];

//self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor yellowColor]};
    
    // set about
    // right bar layout
    UIImage *refreshimage=[UIImage imageNamed:@"menuicon.png"];
    refreshimage=[MapLocations imageWithImage:refreshimage scaledToSize:CGSizeMake(30, 35)];
    
    UIBarButtonItem *about=[[UIBarButtonItem alloc]initWithImage:refreshimage style:UIBarButtonItemStylePlain target:self action:@selector(showActionSheet)];
    self.navigationItem.rightBarButtonItem=about;

    
    
    // nearest branch
    if([callertype isEqualToString:@"nearest"])
    {   manager=[[CLLocationManager alloc]init];
        manager.delegate = self;
        manager.desiredAccuracy = kCLLocationAccuracyKilometer;
        [manager requestAlwaysAuthorization];
        [manager requestWhenInUseAuthorization];
    }
    //    [self.view setBackgroundColor:[[UIColor alloc]initWithRed:0/255 green:103/255 blue:144/255 alpha:.8f]];
    
    types=[[NSArray alloc] initWithObjects:@"جميع التجار",@"مواقع تحصيل وايداع",@"عمليات عالية القيمة",@"تقبل ماستر كارد",
         @"مواقع سوبر فورى", nil
    ,nil];
    
    
    NSArray *codes=[[NSArray alloc]initWithObjects:@"All",@"B2B",@"HighValue",@"MasterCard",@"SuperFawry", nil];
    
    typecode=[[NSDictionary alloc]initWithObjects:codes forKeys:types];
    
    
   
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
   // NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"" message:@"تعذر الوصول لموقعك" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil];
    [errorAlert show];
}




-(void)locationManager:(CLLocationManager *)managers didUpdateLocations:(NSArray *)locations
{
    [self.manager stopUpdatingLocation];
    manager=nil;
    int pos=[locations count];
    pos -=1;
   // NSLog(@"%D",[locations count]);
    CLLocation *currentLocation = [locations objectAtIndex:pos];
    
    if (currentLocation != nil) {
       // NSLog([NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]);
       // NSLog([NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
    
    
    }
    lat=[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    
    lon=[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    [nearesturl appendString:@"&lat="];
    [nearesturl appendString:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]];
    [nearesturl appendString:@"&lon="];
    [nearesturl appendString:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]];
    
    web=[[WebServiceRequests alloc]initWithUrl:nearesturl];
    
    timer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(check) userInfo:self repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
    

    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [types count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellidentifier=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier forIndexPath:indexPath];
    
    
   
    
    cell.textLabel.text=[types objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment=NSTextAlignmentCenter;
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}



-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
 //   [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[[UIColor alloc]initWithRed:1 green:1 blue:0 alpha:1]];
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextAlignment:NSTextAlignmentCenter];
    

return @"اختيار الخدمات";
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(timer!=nil) // this mean that an timer instance is running
        return;
    manager=[[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [manager requestAlwaysAuthorization];
    [manager requestWhenInUseAuthorization];
    
    fawrytype=[typecode objectForKey:[types objectAtIndex:indexPath.row]];
    
    if([callertype isEqualToString:@"targetlocation"])
    {
        
        nearesturl=[[NSMutableString alloc]initWithString:@"http://sacred-store-100115.appspot.com/API/getBranchesByLocationAfterExtraFilters?"];
        [nearesturl appendString:@"type="];
        [nearesturl appendString:[typecode objectForKey:[types objectAtIndex:indexPath.row]]];
        
        
        
        [nearesturl appendString:@"&lat="];
        [nearesturl appendString:lat];
        [nearesturl appendString:@"&lon="];
        [nearesturl appendString:lon];
        
        web=[[WebServiceRequests alloc]initWithUrl:nearesturl];
        
        timer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(check) userInfo:self repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];

        
        
        return;
        
        
    }

    
    if([callertype isEqualToString:@"nearest"])
    {
    
        nearesturl=[[NSMutableString alloc]initWithString:@"http://sacred-store-100115.appspot.com/API/getBranchesByLocationAfterExtraFilters?"];
        [nearesturl appendString:@"type="];
        [nearesturl appendString:[typecode objectForKey:[types objectAtIndex:indexPath.row]]];

        
        [manager startUpdatingLocation];
        
        return;
        
        
    }
    if([callertype isEqualToString:@"address"])
    {
        
        
        
        NSMutableString *searchbyaddress=[[NSMutableString alloc]initWithString:@"http://sacred-store-100115.appspot.com/API/getBranchesByLocationAfterExtraFilters?"];
        [searchbyaddress appendString:@"type="];
        [searchbyaddress appendString:[typecode objectForKey:[types objectAtIndex:indexPath.row]]];
        
        [searchbyaddress appendString:@"&lat="];
        [searchbyaddress appendString:lat];
        [searchbyaddress appendString:@"&lon="];
        [searchbyaddress appendString:lon];
        
        web=[[WebServiceRequests alloc]initWithUrl:searchbyaddress];
        
        timer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(check) userInfo:self repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        
        
        
        return;
        
        
    }
    if([callertype isEqualToString:@"terminalid"])
    {
        
        
        
        NSMutableString *searchbyid=[[NSMutableString alloc]initWithString:@"http://sacred-store-100115.appspot.com/API/findBranchByTerminalNo?"];
        [searchbyid appendString:@"type="];
        [searchbyid appendString:[typecode objectForKey:[types objectAtIndex:indexPath.row]]];
        
        
        web=[[WebServiceRequests alloc]initWithUrl:searchbyid];
        
        timer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(check) userInfo:self repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        
        
        
        return;
        
        
    }
    
    
    // search by city
    
    NSMutableString *tempurl=[[NSMutableString alloc]initWithString:@"http://sacred-store-100115.appspot.com/API/getBranchesAfterExtraFilters2?"];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *city = [defaults objectForKey:@"city"];
    NSString *area = [defaults objectForKey:@"area"];
    NSString *district = [defaults objectForKey:@"district"];
    
    [tempurl appendString:@"city="];
    [tempurl appendString:[city stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ];
    [tempurl appendString:@"&area="];
    [tempurl appendString:[area stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [tempurl appendString:@"&district="];
    [tempurl appendString:[district stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [tempurl appendString:@"&type="];
    [tempurl appendString:[typecode objectForKey:[types objectAtIndex:indexPath.row]]];
    
    
    web=[[WebServiceRequests alloc]initWithUrl:tempurl];
    
    timer=[NSTimer timerWithTimeInterval:1 target:self selector:@selector(check) userInfo:self repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];

    
    
}

-(void)check
{
    if(web.IsLoaded==2)// then error occur
    {
        [timer invalidate];
        timer=nil;
    }
    
    if(web.IsLoaded==1){
        [timer invalidate];
        timer =nil;
        
        
        if([callertype isEqualToString:@"targetlocation"]||[callertype isEqualToString:@"nearest"]||[callertype isEqualToString:@"address"])
 {
     [timer invalidate];
     timer =nil;
     NSArray *locations=[NSJSONSerialization JSONObjectWithData:[web Data] options:kNilOptions error:nil];
     
     if([locations count]==0)
     {
         
         [[UIAlertView alloc]initWithTitle:@"" message:@"لا يوجد فروع." delegate:self cancelButtonTitle:nil otherButtonTitles:@"موافق", nil].show;
         return;
     }
     MapLocations *mapinstance=[self.storyboard instantiateViewControllerWithIdentifier:@"MapLocations"];
     
     [mapinstance setFawrylocations:locations];
     [mapinstance setMylat:lat];
     [mapinstance setMylong:lon];
     [mapinstance setCallertype:callertype];
     [mapinstance setFawrytype:fawrytype];
     
     [self.navigationController pushViewController:mapinstance animated:true];
 
 
     return;}
     
        // if search by city
        
        [timer invalidate];
        timer =nil;
        
        NSDictionary *dict=web.datadic;
        
        NSArray *locations=[NSJSONSerialization JSONObjectWithData:[web Data] options:kNilOptions error:nil];

        if([locations count]==0)
        {
            
            [[UIAlertView alloc]initWithTitle:@"" message:@"لا يوجد فروع." delegate:self cancelButtonTitle:nil otherButtonTitles:@"موافق", nil].show;
        return;
    }
     MapLocations *mapinstance=[self.storyboard instantiateViewControllerWithIdentifier:@"MapLocations"];
    
        [mapinstance setFawrylocations:locations];
        
        [self.navigationController pushViewController:mapinstance animated:true];
        

    }
    
}





-(void)showActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:Nil delegate:self cancelButtonTitle:@"رجوع" destructiveButtonTitle:nil otherButtonTitles:@"الصفحة الرئيسية", @"من نحن ؟", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault; [actionSheet showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case 1:
        {
            about *aboutinstance=[self.storyboard instantiateViewControllerWithIdentifier:@"about"];
            [self.navigationController presentModalViewController:aboutinstance animated:YES];
        }
            break;
            
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
