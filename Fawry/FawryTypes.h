//
//  FawryTypes.h
//  Fawry
//
//  Created by Mohamed Atta on 11/4/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapLocations.h"
#import "WebServiceRequests.h"
#import <CoreLocation/CoreLocation.h>
#import "about.h"
@interface FawryTypes : UITableViewController<CLLocationManagerDelegate,UIActionSheetDelegate>

@property(strong,atomic)NSDictionary *typecode;
@property(strong,atomic)NSArray *types;
@property(strong,atomic)NSTimer *timer;
@property(strong,atomic)WebServiceRequests *web;
@property(strong,atomic)CLLocationManager *manager;
@property(strong,nonatomic)NSString * callertype;
@property(strong,nonatomic)NSString *lon;
@property(strong,nonatomic)NSString *lat;
@property(strong,nonatomic)NSString *terminalid;

@end
