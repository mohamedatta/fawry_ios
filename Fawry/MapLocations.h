//
//  MapLocations.h
//  Fawry
//
//  Created by Mohamed Atta on 11/8/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FawryTypes.h"
#import <CoreLocation/CoreLocation.h>
#import "MyAnnotation.h"
#import "WebServiceRequests.h"
@interface MapLocations : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property(strong,atomic)NSArray * fawrylocations;
@property(strong,nonatomic)NSMutableString *phone;
@property(strong,nonatomic)NSMutableString *merchanttype;
@property (weak, nonatomic) IBOutlet UILabel *note;
@property(strong,nonatomic)NSMutableString *terminalid;
@property(strong,nonatomic)NSString *mylat;
@property(strong,nonatomic)NSString *mylong;
@property(strong,nonatomic)NSString *callertype;
@property(strong,nonatomic)NSString *fawrytype;

@property(strong,atomic)NSTimer *timer;
@property(strong,atomic)WebServiceRequests *web;
@property(strong,atomic)CLLocationManager *manager;
@property(strong)MKAnnotationView *targetpin;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
