//
//  about.h
//  Fawry
//
//  Created by Mohamed Atta on 11/25/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface about : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *text;
- (IBAction)back:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *link1;
@property (weak, nonatomic) IBOutlet UITextView *link2;
@property (weak, nonatomic) IBOutlet UITextView *title2;

@property (weak, nonatomic) IBOutlet UIButton *backbtn;
@property (weak, nonatomic) IBOutlet UITextView *text2;
@end
