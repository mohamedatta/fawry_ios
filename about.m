//
//  about.m
//  Fawry
//
//  Created by Mohamed Atta on 11/25/15.
//  Copyright (c) 2015 Mohamed Atta. All rights reserved.
//

#import "about.h"

@implementation about
@synthesize text,text2,link1,link2,title2,backbtn;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"فوري "];
    NSMutableString *message=[[NSMutableString alloc]initWithString:@" فوري هي شبكة مدفوعات الكترونية رائدة تقدم خدمات مالية للعملاء والشركات خلال قنوات متعددة و اكثر من 50 الف موقع."];


      text2.text=@"مركز الاتصال : 16421\n v1";

    
    
    text.text=message;
    NSURL *URL = [NSURL URLWithString: @"http://fawry.com/ar/%D9%85%D9%86-%D9%86%D8%AD%D9%86/"];
    NSMutableAttributedString * str = [[NSMutableAttributedString alloc] initWithString:@"أعرف أكثر"];
    [str addAttribute: NSLinkAttributeName value:URL range: NSMakeRange(0, str.length)];
    
    link1.attributedText = str;
    [link1 setFont:[UIFont systemFontOfSize:16]];
    link1.linkTextAttributes= @{ NSUnderlineStyleAttributeName: @1,NSForegroundColorAttributeName: [UIColor whiteColor] };
    
    NSURL *URL2 = [NSURL URLWithString: @"http://goo.gl/forms/ld83NgjZtS"];
    NSMutableAttributedString * str2 = [[NSMutableAttributedString alloc] initWithString:@"لإبداء رأيكم"];
    [str2 addAttribute: NSLinkAttributeName value:URL2 range: NSMakeRange(0, str2.length)];
    
    link2.attributedText = str2;
    
    link2.linkTextAttributes= @{ NSUnderlineStyleAttributeName: @1,NSForegroundColorAttributeName: [UIColor whiteColor] };
    
    [link2 setFont:[UIFont systemFontOfSize:16]];
    
    link2.textAlignment=NSTextAlignmentRight;
    link1.textAlignment=NSTextAlignmentRight;
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    int height=screenHeight/8;
    int width=screenWidth*2/3;
    
    int x=10,y=0;// constant for navigation height
    
    
    [self.view setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    [title2 setFrame:CGRectMake(screenWidth/2-40, title2.frame.origin.y,title2.frame.size.width, title2.frame.size.height)];
        [backbtn setFrame:CGRectMake(screenWidth-40, backbtn.frame.origin.y,backbtn.frame.size.width, backbtn.frame.size.height)];
    
    [text setFrame:CGRectMake(x, text.frame.origin.y, screenWidth-20, text.frame.size.height)];
    [link1 setFrame:CGRectMake(x, link1.frame.origin.y, screenWidth-20, link1.frame.size.height)];
    [text2 setFrame:CGRectMake(x, text2.frame.origin.y, screenWidth-20, text2.frame.size.height)];
    [link2 setFrame:CGRectMake(x, link2.frame.origin.y, screenWidth-20, link2.frame.size.height)];
    
    
    

}

- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
  
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self viewWillAppear:YES];

}
@end
