//
//  MyAnnotation.h
//  Fawry
//
//  Created by CL admin on 1/1/16.
//  Copyright (c) 2016 CL admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface MyAnnotation : NSObject<MKAnnotation>
@property (nonatomic) int index;

@end
